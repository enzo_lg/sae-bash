==============================================================================
=Guide d'installation de linux et d'un de environement dse travail convenalbe=
==============================================================================

Pour commencer ce guide s'adresse aux personnes voulant installer linux sur une machine vierge de tout système d'exploitation ou pour un remplacement complet de votre ancien système.

======================
=Instalation de linux=
======================
Pour commencer, vérifier dans votre BIOS que votre PC est Bootable sur une clé USB.

Pour lancer l'installation, branchez votre clé USB et relancez votre PC en allant sur le BIOS.

Allez dans les options de Boot et lancez le pc sur votre clé contenant linux.

Une fois cela fait, votre pc va démarrer sur la clé et lancer l'installation de linux.

Pour commencer, vous allez avoir plusieurs options, dont les plus importantes sont:
	-Ubuntu (le nom de votre version)	
	-Boot form next volume
Ici, nous allons seuleument nous intéresser au premier choix, le deuxième étant pour faire un dual boot.

Après avoir choisi le premier choix s'offrant à vous, vous pourrez choisir vos préférances, comme la langue du système/du clavier, la connection.

Une fois vos préferances choisies, vous allez arriver sur une page appellée "type d'installation".
Vous allez choisir l'option "effacer le disque et installer Xubuntu" (votre version de linux).

Une fois cela effectué, vous pourrez configurer votre securité et enfin linux sera installé.

Nous allons maintenant le compléter. 

================================
=programme et application utile=
================================
	-INSTALATION DE VS CODE-

Pour commancer cette partie, vous allez ouvrir votre navigateur favori et écrire "visual studio code" dans la barre de recherche, vous rendre dans la partie téléchargement 
et cliquer sur le bouton .Deb(deb correspond plus ou moins au .exe de windows)

Une fois vs code (appeller code sur linux) installé, allez dans le menu des extensions (les 4 carrés à gauche de la fenêtre) et installez les extensions suivantes:
	-python
	-java (installer "Extension pack java")
	

	-INSTAlLATION DE GIT-
Pour installer git, ouvrez un terminal de commande et tapez la commande suivante:
sudo apt install git

vous aller devoir ensuite rentrer le mot de passe entré lors de l'installation de linux (pas de panique si vous ne voyez pas que vous écrivez, cela est totalement normal)

	-Installation d'extension
-JDK
Ouvrez votre terminal et rentrez la commande suivante pour installer java jdk
sudo apt install openjdk-11-jdk-headless

-python 
Pour installer des extensions de python, comme la docstring automatique ou pytest, entrez les commandes suivantes dans un terminal.
code --install-extension ms-python.python  (pour pytest)

code --install-extension njpwerner.autodocstring     (pour la docstring)

-docker
 Pour installer docker, entrez les commandes suivantes dans un terminal.
sudo apt-get install docker.io

sudo usermode -aG docker $User

===========
=apparence=
===========

Vous aimez la disposition de mon bureau?

Pour le recopier, faites un clique droit sur le tableau de bord, allez sur "tableau de bord" et cliquez sur "préférence du tableau de bord".
Une fois la page ouverte, cliquez sur "sauvegarde et restauration", cliquez ensuite sur le petit icone de fichier "importer" et ouvrez mon fichier "configuration_tableau de bord".
Ensuite seléctionnez-le dans la liste et cliquez sur les petits engrenages " appliquer la configuration".

Vous pouvez aussi le personnaliser vous même et le rendre à votre image. 
